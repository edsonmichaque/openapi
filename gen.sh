#!/usr/bin/env bash

DATE=$(date +'%Y%m%d%H%M')
cp -rf configs configs-$DATE

if [[ -d gen ]]; then
    rm -rf gen
fi

ROOT=$(pwd)

BUILD_ID=$(date +'%Y-%m-%d-%H%M')

yq '.build[] | [.output, .config, .template] | @csv' config.yml |
    while IFS=, read -r output config template; do
        git clone "git@$1/$2-$output.git" "gen/$2-$output" || exit 1
        cd "gen/$2-$output" || exit 1
        git checkout -b "build/$BUILD_ID" || exit 1
        cd "$ROOT" || exit 1
    done

SPEC=$3

function generate() {
    BUILD=0
    if [[ -f gen/$2/BUILD ]]; then
        OLD_MINOR=$(cat gen/$2/BUILD | cut -d '.' -f 2)
        OLD_BUILD=$(cat gen/$2/BUILD | cut -d '.' -f 3)
        if [ "$OLD_MINOR" == "$DATE" ]; then
            BUILD=$((OLD_BUILD + 1))
        fi
    fi

    sed -i "s|TIMESTAMP|$DATE|g" configs-$DATE/*.json
    sed -i "s|BUILD|$BUILD|g" configs-$DATE/*.json

    docker run --rm -v "${PWD}:/local:Z" -v "$(readlink -f $SPEC):/tmp/openapi.yml:Z" --user "$(id -u):$(id -g)" openapitools/openapi-generator-cli generate \
        -i /tmp/openapi.yml \
        -g "$1" \
        -o "/local/gen/$2" \
        -c "/local/configs-$DATE/$1.json" \
        -t "/local/templates/$1" \
        --api-name-suffix \
        --remove-operation-prefix \
        --api-name-suffix ''

    echo "0.$DATE.$BUILD" >"gen/$2/BUILD"
    sed -i 's/GIT_USER_ID/umbeluzi/g' **/*/*.md
    sed -i "s/GIT_REPO_ID/$2/g" **/*/*.md

    cp "LICENSE-$4" "gen/$2/LICENSE"
}

yq '.build[] | [.output, .config, .template] | @csv' config.yml |
    while IFS=, read -r output config template; do
        generate "$template" "$2-$output" openapi.yml Apache-2.0
    done

sed -r -i 's|json:"(.*)"|json:"\1" yaml:"\1"|g' gen/**/model_*.go

rm -rf configs-$DATE

PARENT=$(pwd)
for i in $(ls gen); do
    cd "gen/$i" || exit 1
    git add :/ || exit 1
    git commit -m "Build $BUILD_ID" || exit 1
    git push origin "build/$BUILD_ID" || exit 1
    cd "$PARENT" || exit 1
done
